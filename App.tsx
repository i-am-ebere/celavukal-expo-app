import { StatusBar } from 'expo-status-bar';
import { QueryClient, QueryClientProvider, useQuery } from 'react-query'
import { StyleSheet } from 'react-native';
import {TailwindProvider} from 'tailwind-rn';
import utilities from './tailwind.json';
import {AppNav} from "./src/nav";
import {AuthProvider} from "./src/services/use-auth";

const queryClient = new QueryClient()

export default function App() {
  return (
      // @ts-ignore-next-line
      <TailwindProvider utilities={utilities}>
          <QueryClientProvider client={queryClient}>
              <AuthProvider>
                  <AppNav />
              </AuthProvider>
          </QueryClientProvider>
      </TailwindProvider>
  );
}
