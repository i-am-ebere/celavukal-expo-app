// import {SafeAreaView} from "react-native-safe-area-context";
import {View, Text, ScrollView, SafeAreaView, ActivityIndicator} from "react-native";
import {WeeklyBarChart} from "./components/weekly-bar-chart";
import {useTailwind} from "tailwind-rn";
import {useAuth} from "../../services/use-auth";
import {Expense} from "./components/expense";
import {useGetUserExpenses, useGetUserExpensesGroupedByWeekAndYear} from "../../services/use-get-user-expenses";
import {convertWeekAndYearToWeekStartDate, groupDateByWeek, IGroupDates} from "../../helpers/expenses";
import {pipe} from "../../helpers/general";
import {IBarChart} from "./types";
import React from "react";
import {IExpense} from "../../services/use-get-user-expenses/types";

export function DashboardScreen() {
    const tailwind = useTailwind()
    const authUser = useAuth()
    const {isLoading, data: expensesData, isError, refetch: expenseRefetching} = useGetUserExpensesGroupedByWeekAndYear()
    const expensesList = (expensesData?.expenses || [])

    function groupSpendingByWeek(expenses: IGroupDates[]):IBarChart {
        return {
            labels:expenses.map((exp) => exp.title.split("-")[0],""),
            data:expenses.map(exp => exp.data.reduce((acc, cur) => acc + cur.amount,0))
        }
    }

    function getFirstSixItems<T>(list:T[]):T[] {
        return list.slice(0, 6)
    }
    if(isLoading) {
        return <ActivityIndicator />
    }

    const recentExpenseList = getFirstSixItems(expensesList.map(exp => exp.expenses.flat()).flat())
    return (
        <SafeAreaView style={tailwind('bg-zinc-100')}>
            <ScrollView showsVerticalScrollIndicator={false} style={[tailwind('mx-5')]}>
            <Text style={tailwind('mb-3 text-lg font-bold')}>Hello, {authUser?.user?.first_name}!</Text>
                <WeeklyBarChart data={pipe(convertWeekAndYearToWeekStartDate,groupSpendingByWeek)(expensesList)} />
            <Text style={tailwind('text-lg font-bold mb-2')}>Expense History</Text>
                {recentExpenseList.map(expense => <Expense key={expense._id} expense={expense} />)}
            </ScrollView>
        </SafeAreaView>
    )
}
