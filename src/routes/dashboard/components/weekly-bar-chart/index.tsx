import { Dimensions, View, Text } from 'react-native'
import {BarChart} from 'react-native-chart-kit'
import {useTailwind} from "tailwind-rn";
import {ChartConfig} from "react-native-chart-kit/dist/HelperTypes";
import {IBarChart} from "../../types";
import React from "react";

const height = 240
const chartConfig: ChartConfig = {
    backgroundColor:"transparent",
    backgroundGradientFrom: '#ffffff',
    backgroundGradientTo: '#ffffff',
    backgroundGradientFromOpacity: 0,
    backgroundGradientToOpacity: 0,
    barRadius: 6,
    color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
}
interface IWeeklyBarChart {
    data: IBarChart
}

export function WeeklyBarChart(props: IWeeklyBarChart) {
    const tailwind = useTailwind()
    const width = Dimensions.get("window").width - 60;

    return (
        <View style={tailwind('mb-3 rounded-lg px-3 bg-gray-200')}>
            <Text style={tailwind('text-center mt-2')}>From Start of Week</Text>
            {/*@ts-ignore-next-line */}
            <BarChart
                fromZero={true}
                withInnerLines={false}
                withHorizontalLabels={false}
                width={width}
                height={height}
                data={{
                    labels: props.data.labels.slice(0, 5),
                    datasets: [{
                        data: props.data.data.reverse().slice(0, 5),
                        colors: props.data.data.map(() => (opacity = 1) => `rgba(134, 65, 244, ${opacity})`).reverse().slice(0, 5),
                    }]
                }}
                chartConfig={chartConfig}
                showBarTops={false}
                withCustomBarColorFromData={true}
                flatColor={true}
                showValuesOnTopOfBars={true}
                style={tailwind('my-2 rounded -mx-1 px-0 text-sm')}
            />
        </View>
    )
}
