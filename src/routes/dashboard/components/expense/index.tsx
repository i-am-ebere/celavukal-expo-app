import {View, Text} from "react-native";
import {useTailwind} from "tailwind-rn";
import dayjs from "dayjs";
import {getDollarAndCents} from "../../../../helpers/currency";
import {IExpense} from "../../../../services/use-get-user-expenses/types";
import {useAuth} from "../../../../services/use-auth";

interface IExpenseProps {
    expense: IExpense
}
export function Expense(props: IExpenseProps) {
    const tailwind = useTailwind()
    const authUser = useAuth()
    const createdAt = dayjs(props.expense.created_at)
    const amountInDollarAndCents = getDollarAndCents(props.expense.amount)
    const isAuthUser = authUser?.user?._id === props.expense.user._id
    return (
        <View style={tailwind("flex-row rounded-md bg-white p-4 mb-2")}>
            <View style={tailwind('mr-2')}>
                <Text style={tailwind('text-base -mb-1')}>{createdAt.format("MMM")} {createdAt.date()}</Text>
                <Text style={tailwind('text-lg font-bold')}>{createdAt.year()}</Text>
            </View>
            <View style={tailwind("flex-row justify-between items-center flex-1")}>
                <View style={tailwind('w-3/5')}>
                    <View style={tailwind('flex-row')}>
                        <Text style={tailwind('text-gray-600 font-bold capitalize text-sm mr-1')}>{props.expense.category.name || "Unknown"}</Text>
                        {!isAuthUser && <Text style={tailwind('text-sm text-gray-700 capitalize')} numberOfLines={1}> - {props.expense.user.firstName} {props.expense.user.lastName}</Text>}
                    </View>
                    <Text style={tailwind('text-lg capitalize')} numberOfLines={1}>{props.expense.description}</Text>
                </View>
                <View style={tailwind('w-2/5')}>
                    <Text style={tailwind('text-right font-bold')}>$<Text style={tailwind('text-2xl')}>{amountInDollarAndCents.dollar}</Text>.{amountInDollarAndCents.cents}</Text>
                </View>
            </View>
        </View>
    )
}
