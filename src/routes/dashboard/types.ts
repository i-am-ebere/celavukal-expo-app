export interface IBarChart {
    labels:string[];
    data:number[];
}
