import {Text, View} from "react-native";
import ComingSoonIcon from "../../assets/svgs/coming-soon-icon.svg"
import {useSafeAreaInsets} from "react-native-safe-area-context";
import {useTailwind} from "tailwind-rn";

export function ProfileScreen() {
    const insets = useSafeAreaInsets()
    const tailwind = useTailwind()
    return (
        <View style={[{top: insets.top, bottom: insets.bottom}, tailwind('justify-center items-center')]} >
            <ComingSoonIcon width={250} height={250} />
            <Text style={tailwind('text-2xl font-bold')}>Coming Soon!</Text>
        </View>
    )
}
