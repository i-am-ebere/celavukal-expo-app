import {ActivityIndicator, Modal, Platform, Pressable, Text, TextInput, View} from "react-native";
import React from "react";
import {useTailwind} from "tailwind-rn";
import * as yup from "yup";
import {Controller, useForm} from "react-hook-form";
import {yupResolver} from "@hookform/resolvers/yup/dist/yup";
import {usePostCategoriesQuery} from "../../../services/use-get-categories";
import {useQueryClient} from "react-query";

interface IAddCategoryProps {
    setModalVisible:(val: boolean) => void;
    modalVisible: boolean;
}

const schema = yup.object({
    name: yup.string().required(),
}).required();


export function AddCategory(props: IAddCategoryProps){
    const tailwind = useTailwind()
    const queryClient = useQueryClient()
    const {mutate, isLoading} = usePostCategoriesQuery()
    const { handleSubmit, control, reset, formState: { errors } } = useForm<{name: string}>({
        resolver: yupResolver(schema),
        mode: "onChange",
        defaultValues:{
            name: ""
        }
    });

    function onSubmit(data: {name:string}) {
        mutate(data, {
            onSuccess:async() => {
                reset()
                await queryClient.invalidateQueries(['getCategoryData'])
                props.setModalVisible(!props.modalVisible)
            },
            onError:() => {

            }
        })
    }

    return (
    <Modal
        animationType="fade"
        transparent={true}
        onRequestClose={()=>{
            reset()
        }}
        visible={props.modalVisible}>
        <Pressable onPress={() => props.setModalVisible(false)} style={tailwind(`${Platform.OS === "ios" ? "bg-slate-600" : "bg-black-inactive"} opacity-70 flex-1`)} />
        <View style={tailwind('bg-white p-3 rounded w-4/5 absolute top-1/3 self-center')}>
            <Text style={tailwind('mb-3 font-bold text-lg')}>Add Category</Text>
            <Controller
                control={control}
                rules={{
                    required: true,
                }}
                render={({ field: { onChange, value } }) => (
                    <>
                        <Text style={tailwind('text-sm -mt-2 font-bold')}>Name</Text>
                        <TextInput
                            autoCapitalize={"none"}
                            onChangeText={onChange}
                            value={value}
                            placeholder={"name"}
                            style={tailwind('border mb-3 bg-white rounded py-3 px-2')} />
                    </>)}
                name={"name"}/>
            {errors.name?.message && <Text style={tailwind('text-red-500 -mt-2 mb-2')}>{errors.name.message}</Text>}
            <Pressable
                disabled={isLoading}
                style={tailwind('px-2 py-2 bg-purple-500 rounded')}
                onPress={handleSubmit(onSubmit)}>
                {isLoading ? <ActivityIndicator style={tailwind('py-2')} color={"white"} /> :<Text style={tailwind('text-white text-center py-2 font-bold')}>Create Category</Text>}
            </Pressable>
        </View>
        {/*</View>*/}
    </Modal>
    )
}
