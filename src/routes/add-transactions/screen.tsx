import {View, TextInput, Text, Pressable, ActivityIndicator} from "react-native";
import {Controller, useForm} from "react-hook-form";
import {yupResolver} from "@hookform/resolvers/yup/dist/yup";
import * as yup from "yup";
import {IUserExpenseRequest} from "../../services/use-get-user-expenses/types";
import {useTailwind} from "tailwind-rn";
import {usePostUserExpenses} from "../../services/use-get-user-expenses";
import React from "react";
import {useFocusEffect, useNavigation} from "@react-navigation/native";
import {useGetCategoriesQuery} from "../../services/use-get-categories";
import DropDownPicker from 'react-native-dropdown-picker';
import {useQueryClient} from "react-query";
import {useSafeAreaInsets} from "react-native-safe-area-context";
import {AddCategory} from "./add-category";

const schema = yup.object({
    description: yup.string().required(),
    amount: yup.number().min(0.01).required(),
    category: yup.string()
}).required();

export function AddTransactionScreen() {
    const {data: categoryData, isLoading: categoryIsLoading} = useGetCategoriesQuery()
    const queryClient = useQueryClient()
    const  {mutate, isLoading: postExpenseIsLoading}= usePostUserExpenses()

    const [modalVisible, setModalVisible] = React.useState(false);
    const [open, setOpen] = React.useState(false);
    const [items, setItems] = React.useState<{label: string; value: string}[]>([]);

    const tailwind = useTailwind()
    const navigation = useNavigation()
    const insets = useSafeAreaInsets()
    const { handleSubmit, control, reset, formState: { errors } } = useForm<IUserExpenseRequest>({
        resolver: yupResolver(schema),
        mode: "onChange",
        defaultValues:{
            description: "",
            amount: 0,
            category: ""
        }
    });
    const isLoading = categoryIsLoading || postExpenseIsLoading

    useFocusEffect(React.useCallback(() => {
       reset()
    }, []))
console.log(categoryData?.categories)
    React.useEffect(() => {
        setItems((categoryData?.categories || []).map(category => ({label: category.name, value: category._id})))
    },[categoryData?.categories])

    function onSubmit(data:IUserExpenseRequest) {
        mutate(data, {
            onSuccess: async () => {
                await queryClient.invalidateQueries({ queryKey: ['getExpenses'] })
                navigation.navigate("Home")
            },
            onError:(err) => {
                console.log("err: ", err?.response?.data)
            }
        })
    }
    return (
        <View style={[tailwind('mx-4'), { marginTop: insets.top, marginBottom: insets.bottom}]}>
            <Controller
                control={control}
                rules={{
                    required: true,
                }}
                render={({ field: { onChange, value } }) => (
                    <>
                    <Text style={tailwind('text-sm -mt-2')}>Amount</Text>
                    <TextInput
                        keyboardType={"numeric"}
                        autoCapitalize={"none"}
                        onChangeText={onChange}
                        value={value}
                        placeholder={"amount"}
                        style={tailwind('border mb-3 bg-white rounded py-3 px-2')} />
                    </>)}
                name={"amount"}/>

            {errors.amount?.message && <Text style={tailwind('text-red-500 -mt-2 mb-2')}>{errors.amount.message}</Text>}
            <Controller
                control={control}
                rules={{
                    required: true,
                }}
                render={({ field: { onChange, value } }) => (
                    <>
                        <Text>Description</Text>
                    <TextInput
                        numberOfLines={3}
                        onChangeText={onChange}
                        value={value}
                        placeholder={"description"}
                        style={tailwind('border mb-3 bg-white rounded py-3 px-2')} />
                        </>
                )}
                name={"description"}/>
            {errors.description?.message && <Text style={tailwind('text-red-500 -mt-2 mb-2')}>{errors.description.message}</Text>}
            <Controller
                control={control}
                rules={{
                    required: true,
                }}
                render={({ field: { onChange, value } }) => (
                    <>
                     <Text>Category</Text>
                    <DropDownPicker
                        open={open}
                        value={value}
                        items={items}
                        setOpen={setOpen}
                        onChangeValue={onChange}
                        setItems={setItems}
                        setValue={onChange}
                    />
                        <Text style={tailwind('font-bold text-blue-500 mt-1')} onPress={() => setModalVisible(true)}>Add Category</Text>
                    </>
                    )}
                name={"category"}/>
            {errors.category?.message && <Text style={tailwind('text-red-500 -mt-2 mb-2')}>{errors.category.message}</Text>}

            <Pressable
                disabled={isLoading}
                onPress={handleSubmit(onSubmit)}
                style={({pressed}) => [
                    tailwind(`${pressed ?  "bg-purple-300" : "bg-purple-500"} mt-3 px-3 py-4 rounded`)
                ]}>
                {isLoading ? <ActivityIndicator color={"white"} /> : <Text style={tailwind('text-center text-white font-bold')}>Add Expense</Text>}
            </Pressable>
            <AddCategory setModalVisible={setModalVisible} modalVisible={modalVisible} />
        </View>
    )
}
