import {Pressable, TextInput, View, Text} from "react-native";
import {useTailwind} from "tailwind-rn";
import {useAuth} from "../../services/use-auth";
import {useForm, Controller} from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";
import {IUserLoginRequest} from "../../services/use-auth/types";
import {useSafeAreaInsets} from "react-native-safe-area-context";

const schema = yup.object({
    email: yup.string().email().required(),
    password: yup.string().required(),
}).required();

export function LoginScreen(){
    const tailwind = useTailwind()
    const authUser = useAuth()
    const insets = useSafeAreaInsets()
    const { handleSubmit, control, formState: { errors } } = useForm<IUserLoginRequest>({
        resolver: yupResolver(schema),
        mode: "onChange",
    });
    function onSubmit(data:IUserLoginRequest){
        authUser?.login(data)
    }
    return (
            <View style={tailwind('flex-1')}>
                <View style={[tailwind('px-4 bg-blue-400 h-4/6'), { paddingTop: insets.top}]} />
                <View style={[tailwind('px-4 bg-white mt-4 h-2/6'), { paddingBottom: insets.bottom}]}>
                    <Controller
                        control={control}
                        rules={{
                            required: true,
                        }}
                        render={({ field: { onChange, value } }) => (
                            <TextInput
                                autoCapitalize={"none"}
                                onChangeText={onChange}
                                value={value||'eiweala@live.com'}
                                placeholder={"email address"}
                                style={tailwind('border mb-3 rounded py-3 px-2')} />)}
                         name={"email"}/>
                    {errors.email?.message && <Text style={tailwind('text-red-500 -mt-2 mb-2')}>{errors.email.message}</Text>}
                    <Controller
                        control={control}
                        rules={{
                            required: true,
                        }}
                        render={({ field: { onChange, value } }) => (
                            <TextInput
                                onChangeText={onChange}
                                value={value}
                                placeholder={"password"}
                                style={tailwind('border mb-3 rounded py-3 px-2')} secureTextEntry={true} />)}
                         name={"password"}/>
                    {errors.password?.message && <Text style={tailwind('text-red-500 -mt-2 mb-2')}>{errors.password.message}</Text>}
                    <Pressable
                        onPress={handleSubmit(onSubmit)}
                        style={({pressed}) => [
                            tailwind(`${pressed ? "bg-blue-500" : "bg-blue-300"} px-3 py-4 rounded`)
                    ]}>
                        <Text style={tailwind('text-center')}>Login</Text>
                    </Pressable>
                </View>
            </View>
    )
}
