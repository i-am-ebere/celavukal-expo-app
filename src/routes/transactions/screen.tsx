import {Pressable, View, Text, SectionList, SafeAreaView, ActivityIndicator} from "react-native";
import {useTailwind} from "tailwind-rn";
import {useAuth} from "../../services/use-auth";
import dayjs from "dayjs";
import weekOfYear from 'dayjs/plugin/weekOfYear'
import isoWeek from 'dayjs/plugin/isoWeek'
import {useGetUserExpenses} from "../../services/use-get-user-expenses";
import {Expense} from "../dashboard/components/expense";
import {groupDateByWeek} from "../../helpers/expenses";
dayjs.extend(isoWeek)
dayjs.extend(weekOfYear)

export function TransactionsScreen() {
    const tailwind = useTailwind()
    const authUser = useAuth()
    const {isLoading, data: expensesData, isError, isRefetching, refetch} = useGetUserExpenses("GET")
    const expensesList = (expensesData?.data.expenses || [])

    if(isLoading) {
        return <ActivityIndicator />
    }
    return (
        <SafeAreaView style={tailwind('mx-4')}>
            <SectionList
                contentContainerStyle={tailwind('grow')}
                showsVerticalScrollIndicator={false}
                refreshing={isRefetching}
                onRefresh={() => refetch()}
                sections={groupDateByWeek(expensesList)}
                keyExtractor={(item, index) => item._id + index}
                renderItem={({item}) => (
                    <Expense expense={item} />
                )}
                renderSectionHeader={({section: {title}}) => (
                    <View style={tailwind('bg-gray-300 rounded px-2')}>
                        <Text style={tailwind('py-2 font-bold')}>{title}</Text>
                    </View>
                )}
            />
        </SafeAreaView>
    )
}
