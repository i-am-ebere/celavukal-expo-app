import {NavigationContainer} from "@react-navigation/native";
import {DashboardScreen} from "./routes/dashboard/screen";
import {createNativeStackNavigator} from "@react-navigation/native-stack";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {TransactionsScreen} from "./routes/transactions/screen";
import {ReportsScreen} from "./routes/reports/screen";
import {AddTransactionScreen} from "./routes/add-transactions/screen";
import {useAuth} from "./services/use-auth";

import HomeIcon from "../src/assets/svgs/home-icon.svg"
import AddExpenseIcon from "../src/assets/svgs/add-expense-icon.svg"
import ReportsIcon from "../src/assets/svgs/reports-icon.svg"
import ProfileIcon from "../src/assets/svgs/profile-icon.svg"
import ExpenseIcon from "../src/assets/svgs/transactions-icon.svg"
import {useTailwind} from "tailwind-rn";
import {LoginScreen} from "./routes/login-screen/screen";
import {ActivityIndicator} from "react-native";
import {bool} from "yup";
import {ProfileScreen} from "./routes/profile-screen/screen";

const AppStack = createNativeStackNavigator();
export function AppNav() {
    const authUser = useAuth()
    if(authUser?.isLoading) {
        return <ActivityIndicator />
    }
    return (
        <NavigationContainer>
        <AppStack.Navigator screenOptions={{
            headerShown: false
        }}>
            {!authUser?.user ?
                <AppStack.Screen name={"AuthNav"} component={AuthRouteNav} />
                :
                <AppStack.Screen name={"SecureNav"} component={SecureRootNav} />
            }
        </AppStack.Navigator>
        </NavigationContainer>
    )
}

const BottomTab = createBottomTabNavigator();
export function SecureRootNav () {
    const tailwind = useTailwind()
    function activeColor(isActive: boolean){
        if(isActive){
            const {backgroundColor } = tailwind("bg-purple-500")
            return backgroundColor as string
        }
        const {backgroundColor } = tailwind("bg-gray-400")
        return backgroundColor as string
    }
    activeColor(true)
    return (
            <BottomTab.Navigator
                initialRouteName="Home"
                screenOptions={{
                    headerShown: false,
                    tabBarShowLabel: false
                }}
                >
                <BottomTab.Screen
                    options={{
                        tabBarShowLabel: false,
                        tabBarIcon:({focused}) => <HomeIcon height={25} fill={activeColor(focused)} />
                    }}
                    name="Home" component={DashboardScreen} />
                <BottomTab.Screen
                    options={{
                        tabBarIcon:({focused}) => <ExpenseIcon height={25} fill={activeColor(focused)}  />
                    }}
                    name="Transactions" component={TransactionsScreen} />
                <BottomTab.Screen
                    options={{
                        tabBarIconStyle: tailwind('-top-5'),
                        tabBarIcon:({focused}) => <AddExpenseIcon height={45} fill={activeColor(focused)}  />
                    }}
                    name="AddTransaction" component={AddTransactionScreen} />
                <BottomTab.Screen
                    options={{
                        tabBarShowLabel: false,
                        tabBarIcon:({focused}) => <ReportsIcon height={25} fill={activeColor(focused)} />
                    }}
                    name="Reports" component={ReportsScreen} />
                <BottomTab.Screen
                    options={{
                        tabBarShowLabel: false,
                        tabBarIcon:({focused}) => <ProfileIcon height={25} fill={activeColor(focused)}  />
                    }}
                    name="Profile" component={ProfileScreen} />
            </BottomTab.Navigator>
    )
}

const AuthAppStack = createNativeStackNavigator();
function AuthRouteNav() {
    return (
        <AuthAppStack.Navigator screenOptions={{
            headerShown: false
        }}>
            <AuthAppStack.Screen name={"Login"} component={LoginScreen} />
            <AuthAppStack.Screen name={"Register"} component={LoginScreen} />
       </AuthAppStack.Navigator>
    )
}
