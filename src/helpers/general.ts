export function pipe(...functions: any[]) {
    return function(value: any){
        return functions.reduce((currentValue, currentFunction) => {
            return currentFunction(currentValue);
        }, value);
    };
}
