import {IExpense, IExpenseGroup, IExpenseGroupByWeekAndYearResponse} from "../services/use-get-user-expenses/types";
import dayjs from "dayjs";

export type IGroupDates = {
    title: string,
    data : IExpense[]
}

export function convertWeekAndYearToWeekStartDate(expenses: IExpenseGroup[]):IGroupDates[] {
    const groupedDates: IGroupDates[] = []
    if(expenses.length === 0){
        return groupedDates
    }

    expenses.forEach((expense) => {
        const startOfWeek = dayjs().day(0).year(expense._id.year).week(expense._id.week).startOf("week")
        const endOfWeek = dayjs().day(0).year(expense._id.year).week(expense._id.week).endOf("week")
        const yearWeek = `${startOfWeek.format("MMM")} ${startOfWeek.date()} - ${endOfWeek.format("MMM")} ${endOfWeek.date()}`;
        // const currentWeekIndex = groupedDates.findIndex(groupDate => groupDate.title === yearWeek)

        groupedDates.push({title: yearWeek, data: expense.expenses})
    });

    return groupedDates
}

export function groupDateByWeek(expenses: IExpense[]):IGroupDates[] {
    const groupedDates: IGroupDates[] = []
    if(expenses.length === 0){
        return groupedDates
    }

    expenses.forEach((expense) => {
        const startOfWeek = dayjs(expense.created_at).startOf("week")
        const endOfWeek = dayjs(expense.created_at).endOf("week")
        const yearWeek = `${startOfWeek.format("MMM")} ${startOfWeek.date()} - ${endOfWeek.format("MMM")} ${endOfWeek.date()}`;
        const currentWeekIndex = groupedDates.findIndex(groupDate => groupDate.title === yearWeek)

        if(currentWeekIndex === -1){
            groupedDates.push({title: yearWeek, data:[expense]})
            return
        }
        groupedDates[currentWeekIndex].data.push(expense)
    });

    return groupedDates
}
