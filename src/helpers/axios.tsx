import axios from "axios";

export const Axios = axios.create({
    baseURL: "https://celavukal.onrender.com/",
    timeout: 10000,
})
