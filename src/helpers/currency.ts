import currency from "currency.js"

export function getDollarAndCents(amount: string | number) {
    const amountInCurrency = currency(amount)
    return {
        dollar: amountInCurrency.dollars(),
        cents: amountInCurrency.cents() < 9 ? `0${amountInCurrency.cents()}` : amountInCurrency.cents()
    }
}
