import {useMutation, useQuery} from "react-query";
import {Axios} from "../../helpers/axios";
import {useAuth} from "../use-auth";
import {AxiosError, AxiosResponse} from "axios";
import {IExpenseGroupByWeekAndYearResponse, IExpenseResponse, IUserExpenseRequest} from "./types";
import {ErrorResponse} from "../types";

export function useGetUserExpenses() {
    const authUser = useAuth()
    return useQuery<AxiosResponse<IExpenseResponse>>("getExpenses", async() => {
        return await Axios.get("/expenses", {
            headers: {
                Authorization: `Bearer ${authUser?.token}`
            }
        })
    })
}

export function useGetUserExpensesGroupedByWeekAndYear() {
    const authUser = useAuth()
    return useQuery<IExpenseGroupByWeekAndYearResponse>("getExpensesGroupByWeekAndYear", async() => {
        const response = await Axios.get("/expenses/group_by", {
            headers: {
                Authorization: `Bearer ${authUser?.token}`
            }
        })

        return response.data
    })
}

export function usePostUserExpenses() {
    const authUser = useAuth()
    return useMutation("postExpenses", async (data: IUserExpenseRequest) => {
        const response = await Axios.post("/expenses", data,{
            headers: {
                Authorization: `Bearer ${authUser?.token}`
            }
        })
        return response.data
    })
}
