export interface IExpenseResponse {
    expenses: IExpense[]
}

export interface IExpenseGroupByWeekAndYearResponse {
    expenses: IExpenseGroup[]
}

export interface IExpenseGroup {
        _id: {
            year: number;
            week: number;
        };
        expenses: IExpense[];
}

export interface IExpense {
    _id: string;
    category: {
        name: string
    };
    user: {
        _id: string;
        firstName: string;
        lastName: string;
    };
    amount: number,
    description: string;
    created_at: string;
    updated_at: string;
}

export interface IUserExpenseRequest {
    description: string;
    amount : number;
    category: string;
}
