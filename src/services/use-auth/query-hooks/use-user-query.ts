import {useMutation, useQuery} from "react-query";
import {Axios} from "../../../helpers/axios";
import {IUser, IUserLoginRequest, IUserLoginResponse} from "../types";
import {ErrorResponse} from "../../types";

export function useUserQuery(token?: string) {
    return useQuery<{user: IUser}, ErrorResponse>("getUserData", async () => {
       const response = await Axios.get("/user", {
            headers:{
                Authorization: `Bearer ${token}`
            }
        })
        return response.data
    }, {
        enabled: !!token
    })
}

export function useUserLoginQuery() {
    return useMutation("useLoginData", async (loginData?: IUserLoginRequest) => {
        const response = await Axios.post("/login", loginData)
        return response.data
    })
}
