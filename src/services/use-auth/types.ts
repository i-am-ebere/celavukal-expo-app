export interface IRegisterUser {
    firstName: string;
    lastName: string;
    email: string;
    phone: string;
    password: string;
}

export interface IUserLoginRequest {
    email: string;
    password: string;
}

export interface IUserLoginResponse {
  token: string;
}

export interface IUser {
    _id: string;
    first_name: string;
    last_name: string;
    email: string;
    is_primary: boolean;
    created_at: string;
    updated_at: string;
    expenses: [];
}
