import React from "react";
import {Axios} from "../../helpers/axios";
import {useUserLoginQuery, useUserQuery} from "./query-hooks/use-user-query";
import * as SecureStore from 'expo-secure-store';
import {IRegisterUser, IUser, IUserLoginRequest} from "./types";


interface IAuthContext {
    user?: IUser;
    token?: string;
    register: (registerUserData: IRegisterUser) => void;
    login: (registerUserData: IUserLoginRequest) => void;
    isLoading: boolean
}

const AuthContext = React.createContext<IAuthContext | null>(null);

export function useAuth() {
    if(!AuthContext) {
        throw "Must be wrapped in AuthProvider to use this hook"
    }
    return React.useContext(AuthContext)
}



export function AuthProvider({children}:{children: React.ReactNode}) {
    const [user, setUser] = React.useState<IUser | undefined>(undefined)
    const [token, setToken] = React.useState<string | undefined>(undefined)
    const {data: userData, isLoading: userIsLoading, error: userQueryError, refetch} = useUserQuery(token)
    const {mutate: mutateUserLogin, isLoading: loginIsLoading} = useUserLoginQuery()
    React.useEffect(() => {
        (async() => {
            try{
                let result = await SecureStore.getItemAsync("userToken");
                if(!result) {
                    return
                }
                setToken(result)
                console.log("got here")
                console.log(userQueryError)
                if(!userData || userQueryError) {
                    return
                }
                setUser(userData.user)
            }catch (e) {
                console.log("e in useEffect with token dep: ", e)
            }
        })()
    },[token])
    console.log("user: ", user)
    async function register(registerUserData: IRegisterUser ) {
        try{
            const registerResponse = await Axios.post("/register", registerUserData)
        }catch (e) {

        }
    }

    async function logout(){
        try{
            await SecureStore.deleteItemAsync("userToken");
            setUser(undefined)
        }catch (e) {

        }
    }
    function login(userLoginData: IUserLoginRequest) {
        mutateUserLogin(userLoginData, {
            onSuccess: async(data) => {
               try{
                 setToken(data.token)
                 await SecureStore.setItemAsync("userToken", data.token);
                 await refetch()
               }catch (e) {

               }
            },
            onError:(err) => {
                console.log(err)
            }
        })
    }

    return (
        <AuthContext.Provider value={{user, token, register, login,  isLoading: userIsLoading || loginIsLoading }}>
            {children}
        </AuthContext.Provider>
    )
}

/**
 * LaDawn VanKirk-The Cost Reduction Specialist.
 * Looking for warm intros to Businesses, families &
 * individuals looking to reduce health care costs.
 * text—724-712-7978—lpn500@yahoo.com.
 * Call/text-Open to F2F conversations.
 * http:calendly.com/ladawnvankirk
 *
 * Celia Powell
 * celiapowellcpa@gmail.com
 * 804-350-4783
 * www.celiapowellcpa.com all social media @celiapowellcpa
 */
