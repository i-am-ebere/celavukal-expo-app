import {useMutation, useQuery} from "react-query";
import {Axios} from "../../helpers/axios";
import {useAuth} from "../use-auth";
import {ICategory} from "./types";

export function useGetCategoriesQuery() {
    const authUser = useAuth()
    return useQuery<{categories: ICategory[]}>("getCategoryData", async () => {
        const response = await Axios.get("/categories", {
            headers:{
                Authorization: `Bearer ${authUser?.token}`
            }
        })
        return response.data
    })
}


export function usePostCategoriesQuery() {
    const authUser = useAuth()
    return useMutation("postCategoryData", async (data: { name: string}) => {
        const response = await Axios.post("/categories", data,{
            headers:{
                Authorization: `Bearer ${authUser?.token}`
            }
        })
        return response.data
    })
}
