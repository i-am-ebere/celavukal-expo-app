import React from "react";
import {IUser} from "../use-auth/types";

interface IUserContext {
    user: IUser;
    token: string;
}

const AuthUserContext = React.createContext<IUserContext | null>(null);

export function useAuthUser() {
    if(!AuthUserContext) {
        throw "AuthUserContext must be set"
    }
    return React.useContext(AuthUserContext)
}

export function AuthUserProvider({user, token, children} ) {
    return (
        <AuthUserContext.Provider value={{user, token}}>
            {children}
        </AuthUserContext.Provider>
    )
}
