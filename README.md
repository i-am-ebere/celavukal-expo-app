### Introduction
This is a simple expense tracking app that tracks purchase by week.
Inspired by an accusation of higher expenses due to high food consumption.

### Screenshots 
![img.png](img.png)
![img_1.png](img_1.png)

### Improvements
- Add reports to give monthly tracking
- Add profile data and feature to invite other house members to user group
- Add registration feature to allow users sign up
- Add test codes for app
